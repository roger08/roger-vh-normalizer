<?php

$virtualhosts = array();

$directory = $argv[1];

$scannedDirectory = array_diff(scandir($directory), array('..', '.'));

foreach ($scannedDirectory as $basePath) {
    $path = $directory . "/" . $basePath;

    if (!is_dir($path) && substr($basePath, -5) == '.json') {
        $identifier = str_replace('.json', '', $basePath);

        if(strlen($identifier) > 1 && strlen($identifier) < 31) {
            $fileContent = file_get_contents($path);

            $virtualhost = json_decode($fileContent, true);

            if (json_last_error() == JSON_ERROR_NONE) {
                $virtualhost['identifier'] = $identifier;
                
                $virtualhosts[] = $virtualhost;
            }
        }        
    }
}
        
file_put_contents($argv[2], json_encode($virtualhosts));
